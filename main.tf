terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.80"
}

provider "yandex" {
  service_account_key_file = "key.json"
  cloud_id                 = "${var.yandex_cloud_id}"
  folder_id                = "${var.yandex_folder_id}"
  zone                     = "ru-central1-a"
}

resource "yandex_vpc_network" "network" {
  name = "my-network"
}

resource "yandex_vpc_subnet" "subnet" {
  name           = "my-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["10.0.1.0/24"]
}

resource "yandex_compute_instance" "ubuntu" {
  count        = 1
  name         = "${count.index == 0 ? "master" : "worker-${count.index}"}"
  zone         = "ru-central1-a"
  platform_id  = "standard-v2"
  boot_disk {
    initialize_params {
      image_id = "fd8emvfmfoaordspe1jr"
      size = 20
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet.id
    nat       = true
  }
  resources {
    cores  = 2
    memory = 2
  }
  metadata = {
    ssh-keys = "ubuntu:${file("./id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "srv" {
  count        = 1
  name         = "srv"
  zone         = "ru-central1-a"
  platform_id  = "standard-v2"
  boot_disk {
    initialize_params {
      image_id = "fd8emvfmfoaordspe1jr"
      size = 20
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet.id
    nat       = true
  }
  resources {
    cores  = 2
    memory = 2
  }
  metadata = {
    ssh-keys = "ubuntu:${file("./id_rsa.pub")}"
  }
}

data "template_file" "hosts_yaml" {
  template = file("hosts.yaml.tpl")

  vars = {
    master_ip = yandex_compute_instance.ubuntu[0].network_interface.0.nat_ip_address
  }
}

data "template_file" "agent" {
  template = file("values.tpl")
}

resource "null_resource" "kubespray_setup" {
  depends_on = [yandex_compute_instance.ubuntu]

  connection {
    type        = "ssh"
    host        = yandex_compute_instance.ubuntu[0].network_interface.0.nat_ip_address
    user        = "ubuntu"
    private_key = file("./id_rsa")
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo DEBIAN_FRONTEND=noninteractive apt-get install -y python3-pip git",
      "export PATH=$PATH:~/.local/bin",
      "git clone https://github.com/kubernetes-sigs/kubespray.git",
      "cd kubespray",
      "git checkout release-${var.kubernetes_release}",
      "sudo pip3 install -r requirements.txt",
      "ssh-keygen -t rsa -b 4096 -N \"\" -f ~/.ssh/id_rsa",
      "ssh-add ~/.ssh/id_rsa",
      "cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "echo '${data.template_file.hosts_yaml.rendered}' > /home/ubuntu/hosts.yaml",
      "cd kubespray",
      "ansible-playbook -i /home/ubuntu/hosts.yaml  --become --become-user=root cluster.yml",
      "cd",
      "curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3",
      "chmod 700 get_helm.sh",
      "./get_helm.sh",
      "sudo helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx",
      "sudo helm repo update",
      "sudo helm install ingress-nginx ingress-nginx/ingress-nginx --namespace  ingress-nginx --create-namespace",
      "sudo helm repo add gitlab https://gitlab.comcloud.xyz",
      "echo '${data.template_file.agent.rendered}' > /home/ubuntu/values.yaml",
      "sudo helm install --namespace default gitlab-runner -f values.yaml gitlab/gitlab-runner",
      "sudo helm upgrade --install ${var.name_space} gitlab/gitlab-agent  --namespace gitlab-agent-${var.name_space}  --create-namespace  --set image.tag=v15.11.0  --set config.token=${var.gitlab_agent_token} --set config.kasAddress=wss://kas.gitlab.com",
      "sudo kubectl create namespace ${var.name_space}",
      "sudo kubectl create secret docker-registry gitlab-credentials --docker-server=registry.gitlab.com --docker-username=${var.user_name} --docker-password=${var.docker_registry_password} --docker-email=${var.user_email} -n ${var.name_space}",
    ]
  }
}
