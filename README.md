<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Readme.md</title>
</head>
<body>
    <h1>Инструкция по развертыванию приложения</h1>
    <ol>
        <li>
            <p>Для установки приложения в Yandex Cloud, создайте сервисный аккаунт и перенесите все данные в файл terraform.tfvars. Создайте GitLab Agent в разделе "Infrastructure" -> "Kubernetes" нового кластера и скопируйте секретный токен. В поле <code>docker_registry_password</code> вставьте access token с правами чтения <code>docker_registry</code>. Создайте <code>runnerRegistrationToken</code>, файл шаблона расположен в файле values.tpl. Перейдите в настройки CI/CD и скопируйте секретный токен.</p>
        </li>
        <li>
            <p>После того, как Terraform установил Kubernetes, перейдите в GitLab и запустите Pipeline. Затем перейдите на сервер, установите Nginx и примените конфигурацию из корневой директории репозитория. Предварительно узнайте IP-адрес сервиса пода с приложением app (sudo kubectl get -n devops services) в Kubernetes, замените его в конфигурации и примените.</p>
        </li>
        <li>
            <p>Настройте логирование: в папке <code>promtail</code> отредактируйте конфигурацию <code>config-map</code>, вставьте внешний IP-адрес сервера мониторинга и примените конфигурацию.</p>
        </li>
        <li>
            <p>На сервере логирования/мониторинга установите Docker Compose. В папке <code>srv/prometheus/.env</code> вставьте токен Telegram. Найдите конфигурацию Prometheus в папке <code>prometheus</code> и добавьте внешний IP-адрес сайта в Kubernetes в секцию <code>job_name: blackbox</code>.</p>
        </li>
        <li>
            <p>В этом разделе описывается функционал Pipeline.</p>
        </li>
        <ul>
    <li><strong>Pre-Build:</strong> В этой стадии выполняются проверки безопасности, такие как обнаружение секретов, SAST и сканирование контейнера.</li>
    <li><strong>Build:</strong> На этом этапе собирается Docker-образ приложения с использованием Kaniko, исходя из Dockerfile в репозитории.</li>
    <li><strong>Post-Build:</strong> После сборки выполняется дополнительное сканирование контейнера.</li>
    <li><strong>Deploy:</strong> На этом этапе происходит развертывание приложения в Kubernetes с использованием Helm.</li>
</ul>
    </ol>
</body>
</html>
