variable "yandex_cloud_id" {
  description = "Yandex Cloud ID"
  type        = string
}

variable "yandex_folder_id" {
  description = "Yandex Folder ID"
  type        = string
}

variable "kubernetes_release" {
  description = "Release kubernetes"
  type        = string
}

variable "name_space" {
  description = "Create namespace"
  type        = string
}

variable "gitlab_agent_token" {
  description = "Token gitlab-agent"
  type        = string
}

variable "user_name" {
  description = "User name gitlab"
  type        = string
}

variable "user_email" {
  description = "Email gitlab"
  type        = string
}

variable "docker_registry_password" {
  description = "docker registry password"
  type        = string
}